﻿using UnityEngine;

public class PrefabCreator
{
    private GameObject m_Prefab;

    public PrefabCreator(int number, GameObject prefab)
    {
        m_Prefab = prefab;
        Create(number);
    }

    public void Create(int number)
    {
        GameObject go = GameObject.Instantiate(m_Prefab);
        go.name = $"Prefab_{number}";
    }
}
