# Bugs  

## Não instanciar o prefab (referência nula)    

No script PrefabCreator.cs, observei que a construtora recebe um segundo parâmetro que é o GameObject a ser instanciado. Como a classe herda da PrimitiveCreator.cs que possui uma construtora com
apenas um argumento, formulei a hipótese que o erro ocorre por causa de nenhum GameObject ser passado como parâmetro. Escrevendo código e realizando testes para resolver isso, percebi que este 
não era o problema. Então formulei uma nova hipótese de que o objeto passado como o prefab podia estar perdendo sua referência ou não ser devidamente referenciado nas chamadas dos métodos. Utilizando
o Debug.Log() da Unity, fui vendo os valores que as variáveis que recebiam a atribuição do prefab continham, e na chamada do método Create pela construtora da classe PrefabCreator, o objeto ficava
com referência nula. A partir disso, fui desenvolvendo a hipótese que a classe pai de PrefabCreator estava chamando o método Create antes que a variável m_Prefab pudesse receber o prefab passado como
parâmetro da construtora. Com isso, comentei as linhas de Instantiate e dei um Debug.Log(m_Prefab) na construtora e no método Create, na construtora retornava o objeto corretamente e em Create, null.
A solução para este bug foi fazer com que a classe PrefabCreator não herdasse a classe PrimitiveCreator e implementar o método Create como sendo da própria classe, sendo chamado na construtora logo
após m_Prefab receber a atribuição.

## StackOverflow ao criar um prefab  

Após resolver o primeiro bug, logo quando criava um prefab pressionando R, o console acusava que estava tendo problema de stack overflow. Como já tinha analisado os scripts previamente e olhado o 
prefab no inspetor da Unity, imaginei que o problema se encontrava no script ObjectRandomize. Minha hipótese era de que este script estava sendo chamado mais de uma vez, já que ele estava adicionando
a ele mesmo como componente dentro da classe e o prefab já vinha com este script como componente no prefab. Para testar, comentei a linha que adicionava o componente ObjectRandomize no objeto que 
continha o script (o prefab). Hipótese confirmada, parou de dar o problema.

## Elemento do enum comentado  

No script TypeEnum.cs, o primeiro elemento do enum estava comentado. Como no gameObject Interaction, que tem na hierarquia da Unity, o enum estava sendo utilizado com 
um valor predeterminado, formulei a hipótese de que, ao descomentar a linha do primeiro elemento, eu teria que fazer alguma alteração no inspetor da Unity. Para isso, 
o teste que realizei foi descomentar a linha e ver o que acontecia ao rodar o projeto. O resultado foi que nenhuma tecla de input estava sendo executada, pois o TypeEnum
no inspetor havia sido modificado. Há duas formas de corrigir esse bug, a primeira é alterar o valor do TypeEnum no inspetor para voltar a se adequar ao código e a segunda
é alterar a condicional no código para se adaptar ao novo valor que apareceu no inspetor. A solução que utilizei foi a primeira.  

## Parâmetro number não sendo passado na criação de esferas  

No script SphereCreator, o parâmetro number não estava sendo passado para a base, com isso, o nome do objeto da cena não estava correto. A correção para este bug foi adicionar o parâmetro number
para a base neste script.